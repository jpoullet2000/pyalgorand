from typing import Optional, List

from algosdk.v2client.algod import AlgodClient
from algosdk.kmd import KMDClient

from ..account import Account
from pyalgorand.connector import SandboxConnector, KMDConnector
from pyalgorand.account import create_account_from_private_address

sbx = SandboxConnector()
sbx.connect()

ALGOD_ADDRESS = sbx.algod_address
ALGOD_TOKEN = sbx.token
# ALGOD_ADDRESS = "http://localhost:4001"
# ALGOD_TOKEN = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"


# def getAlgodClient() -> AlgodClient:
#     return AlgodClient(ALGOD_TOKEN, ALGOD_ADDRESS)

kmd = KMDConnector()
kmd.connect()

KMD_ADDRESS = kmd.algod_address
KMD_TOKEN = kmd.token

# KMD_ADDRESS = "http://localhost:4002"
# KMD_TOKEN = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"


# def get_kmd_client() -> KMDClient:
#     return KMDClient(KMD_TOKEN, KMD_ADDRESS)


KMD_WALLET_NAME = "unencrypted-default-wallet"
KMD_WALLET_PASSWORD = ""

kmd_accounts: Optional[List[Account]] = None


def get_genesis_accounts() -> List[Account]:
    global kmd_accounts

    if kmd_accounts is None:
        # kmd = get_kmd_client()

        wallets = kmd.client.list_wallets()
        walletID = None
        for wallet in wallets:
            if wallet["name"] == KMD_WALLET_NAME:
                walletID = wallet["id"]
                break

        if walletID is None:
            raise Exception("Wallet not found: {}".format(KMD_WALLET_NAME))

        wallet_handle = kmd.client.init_wallet_handle(walletID, KMD_WALLET_PASSWORD)

        try:
            addresses = kmd.client.list_keys(wallet_handle)
            private_keys = [
                kmd.client.export_key(wallet_handle, KMD_WALLET_PASSWORD, addr)
                for addr in addresses
            ]
            # kmd_accounts = [Account(sk) for sk in private_keys]
            kmd_accounts = [create_account_from_private_address(sk) for sk in private_keys]
        finally:
            kmd.client.release_wallet_handle(wallet_handle)

    return kmd_accounts
