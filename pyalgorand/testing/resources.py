from typing import List
from random import choice, randint

from algosdk.v2client.algod import AlgodClient
from algosdk.future import transaction
from algosdk import account

from ..account import Account, create_account_from_private_address
from ..utils import PendingTxnResponse, wait_for_transaction
from .setup import get_genesis_accounts


def pay_account(
    client: AlgodClient,
    sender: Account,
    to: str,
    amount: int
) -> PendingTxnResponse:
    txn = transaction.PaymentTxn(
        sender=sender.getAddress(),
        receiver=to,
        amt=amount,
        sp=client.suggested_params(),
    )
    signedTxn = txn.sign(sender.getPrivateKey())

    client.send_transaction(signedTxn)
    return wait_for_transaction(client, signedTxn.get_txid())


FUNDING_AMOUNT = 100_000_000


def fund_account(
    client: AlgodClient, address: str, amount: int = FUNDING_AMOUNT
) -> PendingTxnResponse:
    funding_account = choice(get_genesis_accounts())
    return pay_account(client, funding_account, address, amount)


account_list: List[Account] = []


def get_temporary_accounts(client: AlgodClient, nb_accounts: int = 8) -> Account:
    global account_list
    if len(account_list) == 0:
        sks = [account.generate_account()[0] for i in range(nb_accounts)]
        account_list = [create_account_from_private_address(sk) for sk in sks]

        genesisAccounts = get_genesis_accounts()
        suggestedParams = client.suggested_params()

        txns: List[transaction.Transaction] = []
        for i, a in enumerate(account_list):
            funding_account = genesisAccounts[i % len(genesisAccounts)]
            txns.append(
                transaction.PaymentTxn(
                    sender=funding_account.public_address,
                    receiver=a.public_address,
                    amt=FUNDING_AMOUNT,
                    sp=suggestedParams,
                )
            )

        txns = transaction.assign_group_id(txns)
        signedTxns = [
            txn.sign(genesisAccounts[i % len(genesisAccounts)].private_key)
            for i, txn in enumerate(txns)
        ]

        client.send_transactions(signedTxns)

        wait_for_transaction(client, signedTxns[0].get_txid())

    return account_list


def get_temporary_account(client: AlgodClient) -> Account:
    return get_temporary_accounts(client, 1)[0]


def opt_into_asset(
    client: AlgodClient, assetID: int, account: Account
) -> PendingTxnResponse:
    txn = transaction.AssetOptInTxn(
        sender=account.getAddress(),
        index=assetID,
        sp=client.suggested_params(),
    )
    signedTxn = txn.sign(account.getPrivateKey())

    client.send_transaction(signedTxn)
    return wait_for_transaction(client, signedTxn.get_txid())


def create_dummy_asset(client: AlgodClient, total: int, account: Account = None) -> int:
    if account is None:
        account = get_temporary_account(client)

    randomNumber = randint(0, 999)
    # this random note reduces the likelihood of this transaction looking like a duplicate
    randomNote = bytes(randint(0, 255) for _ in range(20))

    txn = transaction.AssetCreateTxn(
        sender=account.getAddress(),
        total=total,
        decimals=0,
        default_frozen=False,
        manager=account.getAddress(),
        reserve=account.getAddress(),
        freeze=account.getAddress(),
        clawback=account.getAddress(),
        unit_name=f"D{randomNumber}",
        asset_name=f"Dummy {randomNumber}",
        url=f"https://dummy.asset/{randomNumber}",
        note=randomNote,
        sp=client.suggested_params(),
    )
    signedTxn = txn.sign(account.getPrivateKey())

    client.send_transaction(signedTxn)

    response = wait_for_transaction(client, signedTxn.get_txid())
    assert response.assetIndex is not None and response.assetIndex > 0
    return response.assetIndex
