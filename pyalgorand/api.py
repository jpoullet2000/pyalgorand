from algosdk.v2client.algod import AlgodClient
from pyalgorand.account import Account
from pyalgorand.transactor import PaymentTransactor


def make_x_pays_y(sender: Account, receiver: str, amount: float, client: AlgodClient):
    pt = PaymentTransactor(
        client=client,
        sender_private_key=sender.private_key,
        sender_address=sender.public_address,
        receiver_address=receiver,
        amount=amount)
    return pt.transact()
