from datetime import datetime, timedelta
from pyalgorand.utils import calculate_nb_of_blocks_until_date


def test_calculate_nb_of_blocks_until_date():
    future_datetime = datetime.now() + timedelta(days=1)
    nb_blocks = calculate_nb_of_blocks_until_date(future_datetime)
    assert nb_blocks == 19199
