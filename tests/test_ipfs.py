import pytest
from pathlib import Path
from pyalgorand.ipfs import IPFS


DEFAULT_IPFS_URL = '/ip4/127.0.0.1/tcp/5001'
FILENAME = 'data/sometext.txt'


@pytest.fixture
def ipfs():
    return IPFS(url=DEFAULT_IPFS_URL)


def test_ipfs_add(testdir, ipfs):
    local_filename = testdir / FILENAME
    ipfs.add(local_filename)


def test_ipfs_get_links_from_hash(ipfs):
    info = ipfs.get_links_from_hash('QmPGVbJorgtbuQKBgDP2QJc5Rcv43uZcsJojMM539wA2av')
    print(info)


def test_get(ipfs, tmpdir):
    oo = ipfs.get('QmPGVbJorgtbuQKBgDP2QJc5Rcv43uZcsJojMM539wA2av', tmpdir, 'new_filename.txt')
    assert Path.exists(oo)
