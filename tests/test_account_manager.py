from pyalgorand.account_manager import AccountManager


def test_account_manager(alice_account, tmpdir):
    ACCOUNT_REGISTER = tmpdir / 'account_manager.yml'
    account_manager = AccountManager(filename=ACCOUNT_REGISTER)
    account_manager.add_account(alice_account)
    accounts = account_manager.load_accounts()
    assert 'Alice' in accounts
