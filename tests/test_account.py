from pyalgorand.account import create_account_from_private_address

def test_account_creation(alice_account, algod_client):
    account_info = algod_client.account_info(alice_account.public_address)
    assert account_info['amount'] == 0


def test_create_account_from_private_keys():
    charlie = create_account_from_private_address(
        sk=b'kExlkQReSw5845A0hMfL+4aEH+SNFEZ9ktvP32a7218kZeiw+OKAWobkr5jjOvHQWzsMyoIvPptoN0XM/3+VfA==',
        name='Charlie')
    assert charlie.name == 'Charlie'
    assert charlie.public_address == 'ERS6RMHY4KAFVBXEV6MOGOXR2BNTWDGKQIXT5G3IG5C4Z737SV6CFP47VI'


def test_create_encryption_nacl_keys(alice_account):
    alice_account.create_encryption_nacl_keys()


def test_encrypt_file_with_nacl(alice_account, bob_account, testdir, tmpdir):
    file_to_encrypt = testdir / 'data' / 'sometext.txt'
    encrypted_file = tmpdir / 'encrypted_file'
    alice_nacl_keys = alice_account.create_encryption_nacl_keys()
    bob_nacl_keys = bob_account.create_encryption_nacl_keys()
    _ = alice_account.encrypt_file_with_nacl(
        file_to_encrypt,
        bob_nacl_keys.public_key,
        to_file=encrypted_file
    )
    # Bob creates a second box with his private key to decrypt the message
    with open(file_to_encrypt, 'rb') as f:
        assert bob_account.decrypt_file_with_nacl(encrypted_file, alice_nacl_keys.public_key) == f.read()
