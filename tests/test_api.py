from pyalgorand.api import make_x_pays_y


def test_make_x_pays_y(alice_account, bob_account, algod_client, give_algos_to_alice_and_bob):
    tx = make_x_pays_y(alice_account, bob_account.public_address, 5, algod_client)
