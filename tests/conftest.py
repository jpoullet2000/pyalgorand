from pytest import fixture
import subprocess
from subprocess import PIPE

from pathlib import Path
from tempfile import TemporaryDirectory
from pyalgorand.connector import SandboxConnector, PureStakeConnector
from pyalgorand.account import Account

DEFAULT_NET = 'sandbox'
SANBOX_CMD = 'sandbox/sandbox'


@fixture(scope='module')
def testdir():
    return Path(__file__).parent


@fixture(scope='module')
def tmpdir():
    dir = Path(TemporaryDirectory().name)
    dir.mkdir()
    return dir


@fixture(scope='module')
def connection():
    if DEFAULT_NET == 'testnet':
        con = PureStakeConnector(credentials='tmp/.purestake')
    else:
        con = SandboxConnector(port=4001)
    con.connect()
    return con


@fixture(scope='module')
def algod_client(connection):
    return connection.algod_client


def _create_account_on_testnet(name, testdir):
    acc = Account.from_pickle(testdir / 'data' / 'alice_account.pkl')
    return acc


def _create_account_on_sandbox(name, testdir):
    acc = Account(name=name)
    acc.create()
    return acc


MAP_NET_CREATE_ACCOUNT = {
    'testnet': _create_account_on_testnet,
    'sandbox': _create_account_on_sandbox}


@fixture(scope='module')
def alice_account(testdir):
    return MAP_NET_CREATE_ACCOUNT[DEFAULT_NET](name='Alice', testdir=testdir)


@fixture(scope='module')
def bob_account(testdir):
    return MAP_NET_CREATE_ACCOUNT[DEFAULT_NET](name='Bob', testdir=testdir)


@fixture(scope='module')
def give_algos_to_alice_and_bob(alice_account, bob_account, algod_client):
    import pty
    import os
    amount = 1_000_000_000
    _, slave_fd = pty.openpty()

    cmd = f'{SANBOX_CMD} goal account list'
    p = subprocess.run(
        cmd.split(),
        preexec_fn=os.setsid,
        stdin=slave_fd,
        stdout=PIPE,
        stderr=PIPE,
        universal_newlines=True)
    print(p.stdout)
    sender_address = p.stdout.split('\t')[1]

    # send to Alice account
    cmd = f'{SANBOX_CMD} goal clerk send -a {amount} -f {sender_address} -t {alice_account.public_address}'
    p = subprocess.run(
        cmd.split(),
        preexec_fn=os.setsid,
        stdin=slave_fd,
        stdout=slave_fd,
        stderr=slave_fd,
        universal_newlines=True)

    # send to Bob account
    cmd = f'{SANBOX_CMD} goal clerk send -a {amount} -f {sender_address} -t {bob_account.public_address}'
    p = subprocess.run(
        cmd.split(),
        preexec_fn=os.setsid,
        stdin=slave_fd,
        stdout=slave_fd,
        stderr=slave_fd,
        universal_newlines=True)
    return


class ValueStorage:
    ASSET_ID = 1
