from pyalgorand.testing.resources import get_temporary_account, get_temporary_accounts


def test_get_temporary_account(algod_client):
    acc = get_temporary_account(algod_client)
    assert len(acc.public_address) == 58

def test_get_temporary_accounts(algod_client):
    accs = get_temporary_accounts(algod_client, 8)
    assert len(accs) == 8
