def test_connector(algod_client):
    status = algod_client.status()
    assert status['next-version'] == 'future'
