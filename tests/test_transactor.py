from tests.conftest import ValueStorage
from pyalgorand.transactor import (
    PaymentTransactor,
    AssetConfigTransactor,
    AssetTransferTransactor,
    AssetOptinTransactor,
    # SwapAssetTransactor
)


def test_paymenttransactor(alice_account, bob_account, algod_client, give_algos_to_alice_and_bob):
    tor = PaymentTransactor(
        algod_client,
        sender_private_key=alice_account.private_key,
        sender_address=alice_account.public_address,
        receiver_address=bob_account.public_address,
        amount=100000)
    tor.transact()


class TestAssetTransactor:

    def test_assetconfigtransactor(self, alice_account, bob_account, algod_client, give_algos_to_alice_and_bob):
        tor = AssetConfigTransactor(
            algod_client,
            sender_private_key=alice_account.private_key,
            sender_address=alice_account.public_address,
            total=10_000_000_000,
            default_frozen=False,
            unit_name="ALGOTUTO",
            asset_name="algotuto",
            manager_address=alice_account.public_address,
            reserve_address=bob_account.public_address,
            freeze_address=bob_account.public_address,
            clawback_address=bob_account.public_address)
        _ = tor.transact()
        ASSET_ID = tor.txinfo['asset-index']
        ValueStorage.ASSET_ID = ASSET_ID

    def test_assetoptintransactor(self, alice_account, bob_account, algod_client, give_algos_to_alice_and_bob):
        """Opt-in
        """
        print(ValueStorage.ASSET_ID)
        tor = AssetOptinTransactor(
            algod_client,
            sender_private_key=bob_account.private_key,
            sender_address=bob_account.public_address,
            asset_id=ValueStorage.ASSET_ID,
            amount=0,
        )
        tor.transact()

    def test_assettransfertransactor(self, alice_account, bob_account, algod_client, give_algos_to_alice_and_bob):
        tor = AssetTransferTransactor(
            algod_client,
            sender_private_key=alice_account.private_key,
            sender_address=alice_account.public_address,
            asset_id=ValueStorage.ASSET_ID,
            receiver_address=bob_account.public_address,
            amount=10,
        )
        tor.transact()

    # def test_swapassettransactor(self, alice_account, bob_account, algod_client, give_algos_to_alice_and_bob):
    #     tor = SwapAssetTransactor(
    #         algod_client,
    #         sender_private_key=alice_account.private_key,
    #         sender_address=alice_account.public_address,
    #         asset_id=ValueStorage.ASSET_ID,
    #         asset_amount=1,
    #         microalgo_amount=3000,
    #         ratn=1,
    #         ratd=3000,
    #         min_trade=2999,
    #         max_fee=2000,
    #         fee=0,
    #     )
    #     tor.transact()
    #     print('roeneore')
